# Colour Palette Application #

This document outlines the steps necessary to test the **Colour Palette** application.

### What is this repository for? ###

This repository and the code contained therein are released as a proof of concept as part of **Over** (by **GoDaddy**)'s  coding challenge for candidates to the role of **Senior Android Developer**. Although both the source code and related resources are open to public access with no explicit licensing restrictions, they remain sole property of **Over** and the Developer.

### Version ###
**Version 1.0.0**

### How does it work? ###

The **Colour Palette** application is a colour-picking, colour-matching aid that allows to detect and select colours from any picture and retrieve details about such.

The application allows the user to load a picture either from their device's camera or their internal gallery. Once loaded, the picture can be interacted with by single touch or finger motion; the *RGB* value of the selected picture pixel point is extracted and displayed to the user.

The user can, by means of finger motion, zoom in certain areas of the picture.

The detected colour can be expanded into a full screen where *RGB* components and other details, and complementary colours are displayed.

Finally, the user can copy the said colour to their clipboard.

### Contribution guidelines ###

No external contribution expected on this repository

### Libraries and tools used ###

* **Kotlin** is the language of choice for this application. For more information on the language, check the [Kotlin's official documentation site](https://kotlinlang.org/docs/reference/android-overview.html)
* **Koin** is used for Depency injection; the preference over *Dagger2*, *Kodein*, *Katana* and the more recent *Hilt* was motivated by **Koin**'s simple implementation and solid documentation. For a more in-depth comparison of available dependency injection tools, check the following [article](https://proandroiddev.com/a-dependency-injection-showdown-213339c76515)
* All HTTP/API calls are processed using **Retrofit2**
* Google's own **Gson** is used for parsing and conversion from *JSON* format to *POJO/POCO* (Plain Old Java/Custom Objet)
* An external image-picking library is used to load image from the phone's gallery or capture a picture using their device's camera. More information about this library can be found on its official [Github page](https://github.com/esafirm/android-image-picker)
* Motion gestures are handled by a custom gesture-handling library available at this [Github page](https://github.com/Almeros/android-gesture-detectors)

### Possible, future improvements ###

* The application's graphical interface can be improved with the help of a better design
* Beyond simple documentation, such an interactive game requires a walkthrough or at the very least some tooltip to guide the user through every stage of the game. An interesting *spotlight* library can be found [here](https://github.com/TakuSemba/Spotlight)

#### For further detail and information, contact [arlon.mukeba@gmail.com](mailto:arlon.mukeba@gmail.com)