package app.over.android.interview.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.over.android.interview.model.Payload
import app.over.android.interview.viewmodel.repository.ColorService
import org.koin.core.KoinComponent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ColourViewModel(private val colourService: ColorService) : ViewModel(), KoinComponent {

    var colourRequest: MutableLiveData<Any> = MutableLiveData()

    fun getColourInfo(colourCode: String) {
        colourRequest.postValue(LOADING)

        colourService.run {
            getColorInfo(colourCode).enqueue(object : Callback<Payload> {
                override fun onResponse(call: Call<Payload>, response: Response<Payload>) {
                    response.body()?.let { payload ->
                        colourRequest.value = payload
                    }

                    response.errorBody()?.let {
                        colourRequest.postValue(FAILED)
                    }
                }

                override fun onFailure(call: Call<Payload>, t: Throwable) {
                    colourRequest.postValue(FAILED)
                }
            })
        }
    }

    companion object {
        const val LOADING = "LOADING"
        const val FAILED = "FAILED"
    }
}