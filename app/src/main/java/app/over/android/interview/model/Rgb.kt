package app.over.android.interview.model

data class Rgb(

    val fraction: Fraction, val r: Int, val g: Int, val b: Int, val value: String)