package app.over.android.interview

import android.app.Application
import app.over.android.interview.viewmodel.ColourViewModel
import app.over.android.interview.viewmodel.repository.ColorService
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PaletteApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@PaletteApp)

            modules(listOf(appModule))
        }
    }

    private val appModule = module {

        single<ColorService> {
            val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build()

            retrofit.create(ColorService::class.java)
        }

        viewModel { ColourViewModel(get()) }
    }

    companion object {
        const val BASE_URL = "https://www.thecolorapi.com/"
    }
}