package app.over.android.interview.model

data class XYZ(

    val fraction: Fraction, val value: String, val x: Int, val y: Int, val z: Int)