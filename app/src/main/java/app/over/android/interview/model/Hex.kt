package app.over.android.interview.model

data class Hex(

    val value: String, val clean: String)