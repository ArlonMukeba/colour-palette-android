package app.over.android.interview.view

import android.graphics.Bitmap
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.widget.ImageView
import android.widget.TextView
import app.over.android.interview.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener

fun ImageView.loadImage(url: Any, error: Int = R.drawable.ic_image_not_found, listener: RequestListener<Bitmap>) {
    Glide.with(context).asBitmap().listener(listener).load(url).error(error)
        .diskCacheStrategy(DiskCacheStrategy.NONE).into(this)
}

fun TextView.setColorSpan(colour: Int, entire: String, highlight: String) {
    val index = entire.indexOf(highlight)

    text = SpannableStringBuilder(entire).apply {
        setSpan(ForegroundColorSpan(colour), index, (index + highlight.length), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
}