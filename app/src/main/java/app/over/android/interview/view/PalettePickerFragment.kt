package app.over.android.interview.view

import android.Manifest
import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import app.over.android.interview.R
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.esafirm.imagepicker.features.ImagePicker
import kotlinx.android.synthetic.main.fragment_palette.*

open class PalettePickerFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_palette, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        loadImage()
        tvAdd2Palette.setOnClickListener { adapter.addItem((ColorItem(cmDropper.selectedColour))) }
        rvPalette?.adapter = adapter
    }

    private fun loadImage(url: Any = R.drawable.christmas) {
        ivPalette.loadImage(url, R.drawable.ic_image_not_found, imageListener)
        imageSet = false
        adapter.removeItems()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.palette_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_take_picture -> {
                takePicture()
            }
            R.id.menu_open_gallery -> {
                openGallery()
            }
        }
        return true
    }

    private fun checkPermissions(): Boolean = PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (PermissionChecker.PERMISSION_GRANTED in grantResults) {
            when (requestCode) {
                CAMERA_PERMISSION_REQUEST -> takePicture()
                else -> openGallery()
            }
        }
    }

    private fun takePicture() {
        if (checkPermissions()) {
            ImagePicker.cameraOnly().start(this)
        } else {
            requestPermissions(PERMISSIONS, CAMERA_PERMISSION_REQUEST)
        }
    }

    private fun openGallery() {
        if (checkPermissions()) {
            ImagePicker.create(this).showCamera(false).single().start()
        } else {
            requestPermissions(PERMISSIONS, GALLERY_PERMISSION_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            with(ImagePicker.getFirstImageOrNull(data)) {
                loadImage(uri)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private val adapter = ColorAdapter()

    private val imageListener = object : RequestListener<Bitmap> {
        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean) = false

        override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
            resource?.let {
                cmDropper.resetDropper()
                ivPalette.viewTreeObserver.addOnPreDrawListener(drawListener)
            }
            return false
        }
    }

    private var imageSet: Boolean = false

    private var drawListener = ViewTreeObserver.OnPreDrawListener {
        cmDropper.setAnchorImage(ivPalette)
        true
    }

    class ColorAdapter : RecyclerView.Adapter<ColorViewHolder>() {

        private var items = mutableListOf<ColorItem>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
            return ColorViewHolder(LinearLayout(parent.context).apply {
                val lParams = LinearLayout.LayoutParams(96, 96)
                lParams.setMargins(0, 0, 20, 4)
                layoutParams = lParams
                elevation = 4f
            })
        }

        override fun getItemCount() = items.size

        override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
            holder.bind(items[position])
        }

        fun addItem(item: ColorItem) {
            items.add(0, item)
            notifyDataSetChanged()
        }

        fun removeItems() {
            items = mutableListOf()
            notifyDataSetChanged()
        }
    }

    class ColorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(colorItem: ColorItem) {
            with(itemView.context.getDrawable(R.drawable.item_color_bckgrd)) {
                val background = this?.mutate()
                (background as GradientDrawable).setColor(colorItem.colourInt)
                itemView.background = background
                itemView.transitionName = itemView.context.getString(R.string.trans_colour_item)
                itemView.setOnClickListener { startColorDetailsActivity(colorItem) }
            }
        }

        private fun startColorDetailsActivity(colorItem: ColorItem) {
            val intent = Intent(itemView.context, ColourInfoActivity::class.java)
            intent.putExtra(ColourInfoActivity.ARG_COLOUR, colorItem.colourInt)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                itemView.context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(itemView.context as Activity, itemView, itemView.transitionName)
                    .toBundle())
            } else {
                itemView.context.startActivity(intent)
            }
        }
    }

    data class ColorItem(val colourInt: Int)

    companion object {
        const val CAMERA_PERMISSION_REQUEST = 1928
        const val GALLERY_PERMISSION_REQUEST = 1927

        private val PERMISSIONS =
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    override fun onDestroy() {
        super.onDestroy()
        ivPalette.viewTreeObserver.removeOnPreDrawListener(drawListener)
    }
}