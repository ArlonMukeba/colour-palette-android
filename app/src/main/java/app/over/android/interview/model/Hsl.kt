package app.over.android.interview.model

data class Hsl(

    val fraction: Fraction, val h: Int, val s: Int, val l: Int, val value: String)