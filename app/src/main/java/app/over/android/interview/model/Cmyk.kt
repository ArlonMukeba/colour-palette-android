package app.over.android.interview.model

data class Cmyk(

    val fraction: Fraction, val value: String, val c: Int, val m: Int, val y: Int, val k: Int)