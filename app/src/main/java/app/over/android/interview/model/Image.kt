package app.over.android.interview.model

data class Image(

    val bare: String, val named: String)