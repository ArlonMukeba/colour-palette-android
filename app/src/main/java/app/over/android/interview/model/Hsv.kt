package app.over.android.interview.model

data class Hsv(

    val fraction: Fraction, val value: String, val h: Int, val s: Int, val v: Int)