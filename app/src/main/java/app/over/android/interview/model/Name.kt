package app.over.android.interview.model

data class Name(

    val value: String, val closest_named_hex: String, val exact_match_name: Boolean, val distance: Int)