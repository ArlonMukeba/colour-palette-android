package app.over.android.interview.view

import android.content.Context
import android.graphics.*
import android.graphics.Point
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import androidx.core.view.GestureDetectorCompat
import com.almeros.android.multitouch.MoveGestureDetector

class ColourMagnifier(context: Context, attrs: AttributeSet) : View(context, attrs), MoveGestureDetector.OnMoveGestureListener, GestureDetector.OnGestureListener {

    private var moved: Boolean = false
    private var point: Point = Point(0, 0)
    var selectedColour: Int = -1
    private var anchorImage: Bitmap? = null
    private var moveGestureDetector: MoveGestureDetector = MoveGestureDetector(context, this)
    private var gestureDetector: GestureDetectorCompat = GestureDetectorCompat(context, this)

    private val whiteCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        strokeWidth = BRUSH_SIZE
        color = context.getColor(android.R.color.white)
    }

    private val coloredCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        strokeWidth = BRUSH_SIZE
    }

    private val zoomCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)

    fun setAnchorImage(imageView: ImageView) {
        imageView.layout(0, 0, imageView.measuredWidth, imageView.measuredHeight)
        anchorImage = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        imageView.draw(Canvas(anchorImage!!))
        zoomCirclePaint.shader =
            BitmapShader(anchorImage!!, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
    }

    fun resetDropper() {
        moved = false
        point = Point(0, 0)
        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        if (moved) {
            drawOnCanvas(canvas)
        }
        super.onDraw(canvas)
    }

    private fun drawOnCanvas(canvas: Canvas?) {
        canvas?.let { it ->
            drawColouredCircle(it)
            drawWhiteCircle(it)
            drawZoomedImageCircle(it)
        }
    }

    private fun drawWhiteCircle(canvas: Canvas) {
        canvas.drawCircle(point.x.toFloat(), point.y.toFloat(), OUTER_CIRCLE_SIZE - 16, whiteCirclePaint)
    }

    private fun drawColouredCircle(canvas: Canvas) {
        anchorImage?.let { it ->
            with(it.getPixel(point.x, point.y)) {
                selectedColour = Color.rgb(Color.red(this), Color.green(this), Color.blue(this))
                coloredCirclePaint.color = selectedColour
            }
            canvas.drawCircle(point.x.toFloat(), point.y.toFloat(), OUTER_CIRCLE_SIZE, coloredCirclePaint)
        }
    }

    private fun drawZoomedImageCircle(canvas: Canvas) {
        val matrix = Matrix()
        matrix.reset()
        matrix.setScale(MAGNIFIER_SCALE, MAGNIFIER_SCALE, point.x.toFloat(), point.y.toFloat())
        zoomCirclePaint.shader.setLocalMatrix(matrix)
        canvas.drawCircle(point.x.toFloat(), point.y.toFloat(), (OUTER_CIRCLE_SIZE - 20), zoomCirclePaint)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.apply {
            return moveGestureDetector.onTouchEvent(event).or(gestureDetector.onTouchEvent(event))
        }
        return false
    }

    override fun onDown(e: MotionEvent?): Boolean = true

    override fun onShowPress(e: MotionEvent?) {}

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        e?.let {
            moved = true
            point.set(e.x.toInt(), e.y.toInt())
            invalidate()
            return true
        }

        return false
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean = true

    override fun onLongPress(e: MotionEvent?) {}

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean = true

    override fun onMove(detector: MoveGestureDetector?): Boolean {
        detector?.let {
            val x: Float = point.x + it.focusDelta.x
            val y: Float = point.y + it.focusDelta.y

            point.x = when {
                ((x + OUTER_CIRCLE_SIZE) >= width) -> (width - OUTER_CIRCLE_SIZE).toInt()
                ((x <= OUTER_CIRCLE_SIZE)) -> OUTER_CIRCLE_SIZE.toInt()
                else -> x.toInt()
            }

            point.y = when {
                ((y + OUTER_CIRCLE_SIZE) >= height) -> (height - OUTER_CIRCLE_SIZE).toInt()
                (y <= OUTER_CIRCLE_SIZE) -> OUTER_CIRCLE_SIZE.toInt()
                else -> y.toInt()
            }

            invalidate()
            return true
        }

        return false
    }

    override fun onMoveBegin(detector: MoveGestureDetector?): Boolean = true

    override fun onMoveEnd(detector: MoveGestureDetector?) {}

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        anchorImage = null
    }

    companion object {
        private const val OUTER_CIRCLE_SIZE = 160f
        private const val BRUSH_SIZE = 4f
        private const val MAGNIFIER_SCALE = 3f
    }
}