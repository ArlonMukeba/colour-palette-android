package app.over.android.interview.view

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import app.over.android.interview.R
import app.over.android.interview.model.Cmyk
import app.over.android.interview.model.Hsv
import app.over.android.interview.model.Payload
import app.over.android.interview.model.Rgb
import app.over.android.interview.viewmodel.ColourViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_colour_info.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class ColourInfoActivity : AppCompatActivity() {

    private val viewModel: ColourViewModel by viewModel()
    private lateinit var snackBar: Snackbar
    private var colour: String = DEFAULT_COLOUR

    private var observer = Observer<Any> { request ->
        when {
            request is String && request == ColourViewModel.LOADING -> {
                clLoading.isVisible = true
            }
            request is String && request == ColourViewModel.FAILED -> {
                clLoading.isVisible = false
                removeObserver()
                snackBar.show()
            }
            request is Payload -> {
                clLoading.isVisible = false
                removeObserver()

                with(request) {
                    tvColourName.text = name.value
                    setRGB(rgb, (Color.rgb(rgb.r, rgb.g, rgb.b)))
                    setHSV(hsv, (Color.rgb(rgb.r, rgb.g, rgb.b)))
                    setCMYK(cmyk, (Color.rgb(rgb.r, rgb.g, rgb.b)))
                }
            }
        }
    }

    private fun removeObserver() {
        viewModel.colourRequest.removeObserver(observer)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_colour_info)

        var colourInt = intent.getIntExtra(ARG_COLOUR, -1) as Int
        vColour.setBackgroundColor(colourInt)
        colour = Integer.toHexString(colourInt).substring(2).toUpperCase(Locale.ROOT)
        tvColourCode.text = "#$colour"

        viewModel.colourRequest.observeForever(observer)
        viewModel.getColourInfo(colour)

        tvColourCode.setOnClickListener {
            var clipboard: ClipboardManager =
                (getSystemService(CLIPBOARD_SERVICE) as ClipboardManager).apply {

                    setPrimaryClip(ClipData.newPlainText(getString(R.string.colour_copied_msg), tvColourCode.text))
                }

            Toast.makeText(this, R.string.colour_copied_msg, Toast.LENGTH_SHORT).show()
        }

        snackBar =
            Snackbar.make(clParent, R.string.network_error, Snackbar.LENGTH_INDEFINITE).apply {
                setAction(R.string.retry_add_btn) {
                    dismiss()
                    viewModel.getColourInfo(colour)
                }.setBackgroundTint(resources.getColor(android.R.color.holo_red_dark))
                    .setTextColor(resources.getColor(android.R.color.white))
                    .setActionTextColor(resources.getColor(R.color.colorPrimaryDark))
            }
    }

    private fun setRGB(rgb: Rgb, colourInt: Int) {
        with(rgb) {
            tvRComponent.setColorSpan(colourInt, "R: $r", "R")
            tvGComponent.setColorSpan(colourInt, "G: $g", "G")
            tvBComponent.setColorSpan(colourInt, "B: $b", "B")

            (Color.rgb(255 - r, 255 - g, 255 - b)).let { colourInt ->
                (tvCompColour as TextView).setTextColor(colourInt)

                tvCompColour.text = "#${
                    java.lang.Integer.toHexString(colourInt).substring(2).toUpperCase()
                }"

                (getDrawable(R.drawable.item_color_bckgrd)).let { drawable ->
                    val background = drawable?.mutate()
                    (background as GradientDrawable).setColor(colourInt)
                    vCompColour.background = background
                    vCompColour.isVisible = true
                }
            }
        }
    }

    private fun setHSV(hsv: Hsv, colourInt: Int) {
        with(hsv) {
            tvHComponent.setColorSpan(colourInt, "H: $h", "H")
            tvSComponent.setColorSpan(colourInt, "S: $s", "S")
            tvVComponent.setColorSpan(colourInt, "V: $v", "V")
        }
    }

    private fun setCMYK(cmyk: Cmyk, colourInt: Int) {
        with(cmyk) {
            tvCComponent.setColorSpan(colourInt, "C: $c", "C")
            tvMComponent.setColorSpan(colourInt, "M: $m", "M")
            tvYComponent.setColorSpan(colourInt, "Y: $y", "Y")
            tvKComponent.setColorSpan(colourInt, "K: $k", "K")
        }
    }

    override fun onResume() {
        super.onResume()
        clLoading.isVisible = false
    }

    companion object {
        const val ARG_COLOUR = "ARG_COLOUR"
        const val DEFAULT_COLOUR = "ffffff"
    }
}