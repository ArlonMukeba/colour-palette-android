package app.over.android.interview.model

data class Fraction(

    val x: Double, val y: Double, val z: Double)