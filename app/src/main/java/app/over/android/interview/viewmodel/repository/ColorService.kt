package app.over.android.interview.viewmodel.repository

import app.over.android.interview.model.Payload
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ColorService {

    @GET("id")
    fun getColorInfo(@Query(value = "hex") lat: String = "24B1E0"): Call<Payload>
}