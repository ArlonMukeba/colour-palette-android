package app.over.android.interview.model

data class Payload(

    val hex: Hex, val rgb: Rgb, val hsl: Hsl, val hsv: Hsv, val name: Name, val cmyk: Cmyk, val xYZ: XYZ, val image: Image, val contrast: Contrast, val _links: _links, val _embedded: _embedded)