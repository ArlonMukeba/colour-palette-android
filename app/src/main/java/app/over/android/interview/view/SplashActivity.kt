package app.over.android.interview.view

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import app.over.android.interview.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)
        animateLogo()
    }

    private fun animateLogo() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.logo_anim)
        animation.fillAfter = true
        animation.setAnimationListener(AnimationListener())
        cvLogo.startAnimation(animation)
    }

    inner class AnimationListener : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {}
        override fun onAnimationStart(animation: Animation?) {}
        override fun onAnimationEnd(animation: Animation?) {
            moveToPalettePicker()
        }
    }

    private fun moveToPalettePicker() {
        tvLabel.isVisible = true

        val runnable = Runnable {
            val intent =
                Intent(this, PalettePickerActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this, cvLogo, cvLogo.transitionName)
                    .toBundle())
            } else {
                startActivity(intent)
            }
        }

        Handler().postDelayed(runnable, 1000)
    }
}